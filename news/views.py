# coding=utf-8
from django.contrib.auth.models import User
from django.shortcuts import render, get_object_or_404, redirect, HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import permission_required, login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from models import News, City, Categoryitem, Tag, Comment
from PIL import Image
import datetime


def handle_uploaded_file(f, name):
    destination = open('/tmp/' + name, 'wb+')
    for chunk in f.chunks():
        destination.write(chunk)
    destination.close()
    return name


@permission_required('news.add_news', login_url='/reg/')
def admin(request):
    c = Categoryitem.objects.select_related().filter(parent=None)
    t = Tag.objects.all()
    n = News.objects.all().order_by('-id')
    ct = City.objects.all()
    context = {'news': n,
               'tags': t,
               'citys': ct,
               'cats': c}
    return render(request, 'news/admin.html', context)


# @permission_required('news.add_news', login_url='/reg/')
# def xadmin(request):
#     c = Categoryitem.objects.select_related().filter(parent=None)
#     t = Tag.objects.all()
#     n = News.objects.all().order_by('-id')
#     ct = City.objects.all()
#     context = {'title': '| Администрирование новостей',
#                'news': n,
#                'tags': t,
#                'citys': ct,
#                'cats': c}
#     return render(request, 'news/xadmin.html', context)


# def index(request, page='1'):
#     if "cat" in request.COOKIES:
#         if request.COOKIES["cat"] == '0':
#             n = News.objects.all().order_by('-id')
#             cname = 'Подмосковья'
#         else:
#             n = News.objects.all().order_by('-id').filter(category__in=[request.COOKIES["cat"]])
#             cname = 'Электростали'
#     else:
#         n = News.objects.all().order_by('-id')
#         cname = 'Подмосковья'
#     c = Categoryitem.objects.select_related().filter(parent=None)
#     paginator = Paginator(n, 5)
#     try:
#         n = paginator.page(page)
#     except PageNotAnInteger:
#         n = paginator.page(1)
#     except EmptyPage:
#         n = paginator.page(paginator.num_pages)
#     context = {'title': ' Новости Подмосковья ',
#                'news': n,
#                'cats': c,
#                'cat': cname}
#     return render(request, 'news/index.html', context)


def index(request, page='1'):
    from_date = datetime.datetime.now() - datetime.timedelta(days=2)
    l2dn = News.objects.all().order_by('-date').filter(date__range=[from_date, datetime.datetime.now()])
    n = News.objects.all().order_by('-date')
    c = Categoryitem.objects.select_related().filter(parent=None)
    paginator = Paginator(n, 5)
    try:
        n = paginator.page(page)
    except PageNotAnInteger:
        n = paginator.page(1)
    except EmptyPage:
        n = paginator.page(paginator.num_pages)
    context = {'news': n,
               'cats': c,
               'last_news': l2dn}
    return render(request, 'news/index.html', context)


# def sp(request, id='1'):
#     if request.method == "POST":
#         text = request.POST['text']
#         user = User.objects.get(username=request.user)
#         nn = get_object_or_404(News, id=id)
#         nc = Comment()
#         nc.owner = user
#         nc.text = text
#         nc.news = nn
#         nc.save()
#         return redirect(request.path)
#     n = get_object_or_404(News, id=id)
#     c = Categoryitem.objects.select_related().filter(parent=None)
#     com = Comment.objects.select_related().filter(news=n, parent=None)
#     context = {'title': ' Новостная запись ',
#                'news': n,
#                'cats': c,
#                'comments': com}
#     return render(request, 'news/sp.html', context)


def sp(request, id='1'):
    if request.method == "POST":
        text = request.POST['text']
        user = User.objects.get(username=request.user)
        nn = get_object_or_404(News, id=id)
        nc = Comment()
        nc.owner = user
        nc.text = text
        nc.news = nn
        nc.save()
        return redirect(request.path)
    from_date = datetime.datetime.now() - datetime.timedelta(days=2)
    l2dn = News.objects.all().order_by('-date').filter(date__range=[from_date, datetime.datetime.now()])
    ln = News.objects.all()[:5]
    ct = City.objects.all()
    n = get_object_or_404(News, id=id)
    c = Categoryitem.objects.select_related().filter(parent=None)
    com = Comment.objects.select_related().filter(news=n, parent=None)
    context = {'news': n,
               'cats': c,
               'citys': ct,
               'comments': com,
               'lnews': ln,
               'last_news': l2dn}
    return render(request, 'news/sp.html', context)


@permission_required('news.add_news', login_url='/reg/')
def add_news(request):
    imgroot = '/var/pj/gk/st/news/'
    imgroot54x54 = '/var/pj/gk/st/news/54x54/'
    if request.method == "POST":
        title = request.POST['news_name']
        tags = request.POST['news_tags']
        active = request.POST['news_status']
        category = request.POST['news_category']
        sweight = request.POST['news_priority']
        date = request.POST['news_date']
        text = request.POST['news_content']
        n = News()
        if 'news_autor' in request.POST:
            n.autor = request.POST['news_autor']
        if 'news_photo_autor' in request.POST:
            n.photo_title = request.POST['news_photo_autor']
        if 'news_origin' in request.POST:
            n.origin = request.POST['news_origin']
        if 'news_city' in request.POST:
            n.city = request.POST['news_city']
        if 'news_media' in request.POST:
            n.media = request.POST['news_media']
        user = User.objects.get(username=request.user)
        n.owner = user
        n.title = title
        if active == 'on':
            active = True
        else:
            active = False
        n.active = active
        n.text = text
        n.sweight = sweight
        n.date = date
        n.save()
        tagsl = tags.split(',')
        tagsitems = []
        for item in tagsl:
            if len(item.strip()):
                tagsitems.append(Tag.objects.get_or_create(name=item.strip())[0].id)
        n.tags = tagsitems
        categoryl = category.split(',')
        categoryitems = []
        for item in categoryl:
            if len(item.strip()):
                categoryitems.append(item)
        n.category = categoryitems
        tmpfile = handle_uploaded_file(request.FILES['news_logo'], str(n.id) + request.FILES['news_logo'].name[-4:])
        img = Image.open('/tmp/' + tmpfile)
        img.save(imgroot + str(n.id) + '.png', 'PNG')
        img.thumbnail([54, 54], Image.ANTIALIAS)
        img.save(imgroot54x54 + str(n.id) + '.png', 'PNG')
        request.session['alert'] = 'Новость успешно добавлена'
        return redirect('/news/admin/')


@permission_required('news.change_news', login_url='/reg/')
def edit_news(request):
    if request.method == "POST":
        title = request.POST['edit_news_name']
        tags = request.POST['edit_news_tags']
        id = request.POST['edit_news_id']
        text = request.POST['edit_news_content']
        category = request.POST['edit_news_cat']
        n = News.objects.get(id=id)
        n.title = title
        n.text = text
        n.save()
        tagsl = tags.split(',')
        tagsitems = []
        for item in tagsl:
            tagsitems.append(Tag.objects.get_or_create(name=item.strip())[0].id)
        n.tags = tagsitems
        categoryl = category.split(',')
        categoryitems = []
        for item in categoryl:
            if len(item.strip()):
                categoryitems.append(item)
        n.category = categoryitems
        request.session['alert'] = 'Новость успешно отредактирована'
        return redirect('/news/sp/' + id)


@permission_required('news.add_categoryitem', login_url='/reg/')
def add_category(request):
    if request.method == "POST":
        name = request.POST['cat_name']
        parent = request.POST['parent_cat']
        c = Categoryitem()
        if parent != '0':
            pc = get_object_or_404(Categoryitem, id=parent)
            c.parent = pc
        c.name = name
        c.save()
        request.session['alert'] = 'Категория успешно добавлена'
        return redirect('/news/admin/')


def addcomment(request):
    if request.method == "POST":
        news = News.objects.get(id=request.POST['news'])
        text = request.POST['text']
        user = User.objects.get(username=request.user)
        c = Comment()
        c.news = news
        c.owner = user
        c.text = text
        if request.POST['pid'] != 'false':
            pid = request.POST['pid']
            c.parent = Comment.objects.get(id=pid)
        c.save()
        return HttpResponse(c.id)


@permission_required('news.delete_categoryitem', login_url='/reg/')
def remcat(request):
    if request.method == "POST":
        cats = request.POST['category']
        catsl = cats.split(',')
        for item in catsl:
            c = Categoryitem.objects.get(id=item)
            c.delete()
        request.session['alert'] = 'Категория(и) успешно удалены.'
        return redirect('/news/admin/')


@permission_required('news.delete_news', login_url='/reg/')
def remnews(request):
    if request.method == "POST":
        news = request.POST['news']
        newsl = news.split(',')
        for item in newsl:
            n = News.objects.get(id=item)
            n.delete()
        request.session['alert'] = 'Новость(и) успешно удалены.'
        return redirect('/news/admin/')


@permission_required('news.delete_tag', login_url='/reg/')
def remtags(request):
    if request.method == "POST":
        tags = request.POST['tags']
        tagsl = tags.split(',')
        for item in tagsl:
            t = Tag.objects.get(id=item)
            t.delete()
        request.session['alert'] = 'Тег(и) успешно удалены.'
        return redirect('/news/admin/')

