# coding=utf-8
from django.db import models
from django.contrib.auth.models import User
from gk.models import City


class Tag(models.Model):
    name = models.CharField(max_length=300, verbose_name=u'Метка')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u"Тег"
        verbose_name_plural = u"Теги"


class Categoryitem(models.Model):
    parent = models.ForeignKey('self', verbose_name=u'Категория родитель', blank=True, null=True)
    name = models.CharField(max_length=200, verbose_name=u'Название')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u"Категория"
        verbose_name_plural = u"Категории"


class News(models.Model):
    active = models.BooleanField(verbose_name=u'Опубликовано')
    owner = models.ForeignKey(User, verbose_name=u'Пользователь')
    title = models.CharField(max_length=500, verbose_name=u'Заголовок')
    text = models.TextField(verbose_name=u'Текст новости')
    tags = models.ManyToManyField(Tag, verbose_name=u'Теги')
    category = models.ManyToManyField(Categoryitem, verbose_name=u'Категория')
    date = models.DateTimeField(verbose_name=u'Дата публикации')
    autor = models.CharField(max_length=500, blank=True, null=True, verbose_name=u'Автор')
    origin = models.CharField(max_length=500, blank=True, null=True, verbose_name=u'Источник')
    photo_title = models.CharField(max_length=500, blank=True, null=True, verbose_name=u'Источник фото')
    media = models.CharField(max_length=4000, blank=True, null=True, verbose_name=u'Медиа контент')
    sweight = models.DecimalField(max_digits=3, decimal_places=2, default=0, verbose_name=u'Поимсковый вес')
    city = models.ForeignKey(City, verbose_name=u'Город', blank=True, null=True)


def __unicode__(self):
    return self.title


class Meta:
    verbose_name = u"Новость"
    verbose_name_plural = u"Новости"


class Comment(models.Model):
    news = models.ForeignKey(News, verbose_name=u'Новость')
    date = models.DateField(auto_now_add=True, verbose_name=u'Дата')
    parent = models.ForeignKey('self', verbose_name=u'Коментарий родитель', blank=True, null=True)
    owner = models.ForeignKey(User, verbose_name=u'Пользователь')
    text = models.TextField(verbose_name=u'Текст коментария')