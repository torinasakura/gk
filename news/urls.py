from django.conf.urls import patterns, url


urlpatterns = patterns('',
                       url(r'^$', 'news.views.index', name='news'),
                       # url(r'^xindex/', 'news.views.xindex'),
                       url(r'^page/(?P<page>\d+)/', 'news.views.index'),
                       # url(r'^xpage/(?P<page>\d+)/', 'news.views.xindex'),
                       url(r'^admin/addnews/', 'news.views.add_news'),
                       url(r'^admin/editnews/', 'news.views.edit_news'),
                       url(r'^admin/addcategory/', 'news.views.add_category'),
                       url(r'^admin/remcategory/', 'news.views.remcat'),
                       url(r'^admin/remnews/', 'news.views.remnews'),
                       url(r'^admin/remtags/', 'news.views.remtags'),
                       url(r'^admin/', 'news.views.admin'),
                       # url(r'^xadmin/', 'news.views.xadmin'),
                       url(r'^addcomment/', 'news.views.addcomment'),
                       url(r'^sp/(?P<id>\d+)/', 'news.views.sp'),
                       # url(r'^xsp/(?P<id>\d+)/', 'news.views.xsp'),
)