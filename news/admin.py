from django.contrib import admin
from news.models import Tag, Categoryitem, News, Comment

admin.site.register(Tag)
admin.site.register(Categoryitem)
admin.site.register(News)
admin.site.register(Comment)