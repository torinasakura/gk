﻿from django.shortcuts import render
from news.models import News
from gk.models import Favcam
from django.contrib.auth.decorators import login_required


@login_required(login_url='/reg/')
def index(request):
    favcam = Favcam.objects.get_or_create(user=request.user)[0]
    favcams = favcam.cams.all()
    n = News.objects.all().order_by('-id')[:4]
    context = {'news': n,
               'cams': favcams, }
    return render(request, 'cp/index.html', context)
