from django.conf.urls import patterns, include, url
from django.contrib import admin
from sitemap import Newss, Statics, StaticIndex

sitemaps = {'Index': StaticIndex,
            'Static': Statics,
            'News': Newss}
admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^$', 'gk.views.index', name='index'),
                       url(r'^el/', 'gk.views.el', name='about'),
                       url(r'^about/', 'gk.views.about', name='about'),
                       url(r'^contacts/', 'gk.views.contacts', name='contacts'),
                       url(r'^reg/', 'gk.views.reg', name='reg'),
                       url(r'^fpass/', 'gk.views.fpass'),
                       url(r'^snpass/(?P<key>\w+)/', 'gk.views.snpass'),
                       url(r'^login/', 'gk.views.loginv'),
                       url(r'^logout/', 'gk.views.logoutv'),
                       url(r'^emailv/(?P<key>\w+)/', 'gk.views.email_verification'),
                       url(r'^ads/', 'gk.views.ads', name='ads'),
                       url(r'^allcam/', 'gk.views.allcam'),
                       url(r'^elallcam/', 'gk.views.allcam'),
                       url(r'^lastcameras/', 'gk.views.lastcameras'),
                       url(r'^topcamera/', 'gk.views.topcamera'),
                       url(r'^getcity/', 'gk.views.getcity'),
                       url(r'^topcameras/(?P<step>\d+)/', 'gk.views.topcameras'),
                       url(r'^get_camera-(?P<camera_id>\d+).json$', 'gk.views.get_camera'),
                       url(r'^vote-(?P<camera_id>\d+).json$', 'gk.views.vote'),
                       url(r'^addfav/(?P<camera_id>\d+)/$', 'gk.views.addfav'),
                       url(r'^rmfav/(?P<camera_id>\d+)/$', 'gk.views.rmfav'),
                       url(r'^favcams/', 'gk.views.favcams'),
                       #CP
                       url(r'^cp/', include('cp.urls')),
                       #news
                       url(r'^news/', include('news.urls')),
                       #Admin
                       url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
                       url(r'^admin/', include(admin.site.urls)),
                       #advertising test
                       url(r'^adver/', 'gk.views.adver'),
                       (r'^sitemap.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps}),

                       #Copy
                       # url(r'^xindex/', 'gk.views.xindex', name='xindex'),
                       # url(r'^xabout/', 'gk.views.xabout', name='xabout'),
                       # url(r'^xads/', 'gk.views.xads', name='xads'),
                       # url(r'^xcontacts/', 'gk.views.xcontacts', name='xcontacts'),
)

