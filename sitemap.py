from django.contrib.sitemaps import Sitemap
from django.core.urlresolvers import reverse
from news.models import News


class Newss(Sitemap):
    changefreq = 'daily'
    priority = 0.60

    def items(self):
        return News.objects.filter(active=True)

    def lastmod(self, obj):
        return obj.date

    def location(self, obj):
        return "/news/sp/%s/" % obj.id


class Statics(Sitemap):
    changefreq = 'monthly'

    def priority(self, item):
        return item[1]

    def items(self):
        return [['about', 0.9], ['ads', 0.8], ['contacts', 0.7], ['reg', 0.5], ['news', 0.6]]

    def location(self, item):
        return reverse(item[0])


class StaticIndex(Sitemap):
    priority = 1
    changefreq = 'daily'

    def items(self):
        return ['index']

    def location(self, item):
        return reverse(item)