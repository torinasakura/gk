from django import template
from gk.models import Favcam, Pcam

register = template.Library()


@register.assignment_tag
def fpcams(user):
    fc = Favcam.objects.filter(user=user).count()
    pc = Pcam.objects.filter(user=user).count()
    return fc, pc