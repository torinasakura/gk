from django import template

register = template.Library()


@register.inclusion_tag('tt/omenu.html')
def omenu(path):
    #path = "/"
    return {'path': path}


@register.inclusion_tag('tt/menu.html')
def menu(path, user):
    #path = "/"
    return {'path': path,
            'user': user}
