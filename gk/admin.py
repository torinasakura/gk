from django.contrib import admin
from gk.models import Camera, City, Emailv, Favcam, Rpass

admin.site.register(Camera)
admin.site.register(City)
admin.site.register(Emailv)
admin.site.register(Favcam)
admin.site.register(Rpass)