﻿# -*- coding: utf-8 -*-
import json
from hashlib import md5
import django.core.mail
from decimal import Decimal
from models import Camera, City, Emailv, Favcam, Rpass
from news.models import News
from django.contrib.auth.models import User
from django.shortcuts import render, get_object_or_404, redirect, HttpResponseRedirect
from django.http import HttpResponse
from django.contrib.auth import logout, authenticate, login
from django.core.urlresolvers import reverse
from django.core.mail import send_mail
from django.utils.translation import ugettext_lazy as _


class DecimalEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Decimal):
            return float(obj)
        return json.JSONEncoder.default(self, obj)


def email_verification(request, key):
    mkey = get_object_or_404(Emailv, key=key)
    user = mkey.user
    user.is_active = True
    user.save()
    mkey.delete()
    #fixme
    return HttpResponse(1)


def get_camera(request, camera_id):
    camera = get_object_or_404(Camera, id=camera_id)
    camera.views += 1
    camera.save()
    jsondict = {"id": camera.id}
    return HttpResponse(json.dumps(jsondict), mimetype="application/json")


# def oindex(request):
#     n = News.objects.all().order_by('-id')[:3]
#     cameras = Camera.objects.order_by('-views', 'rate').filter(active=True)[:30]
#     context = {'cameras': cameras,
#                'title': ' Главная ',
#                'news': n, }
#     return render(request, 'index.html', context)


def el(request):
    n = News.objects.all().order_by('-id').filter(category__in=[9])[:3]
    cameras = Camera.objects.order_by('-views', 'rate').filter(active=True)[:30]
    context = {'cameras': cameras,
               'title': ' Главная ',
               'news': n, }
    return render(request, 'el.html', context)


def index(request):
    n = News.objects.all().order_by('-id')[:3]
    c = City.objects.all().filter(active=True)
    cameras = Camera.objects.order_by('-views', 'rate').filter(active=True)[:30]
    jdict = []
    if request.user.is_authenticated():
        favcam = Favcam.objects.get_or_create(user=request.user)[0]
        favcams = favcam.cams.all()
        for camera in cameras:
            if camera in favcams:
                jdict.append({"id": camera.id, "address": camera.address, "direct": camera.direct, "x": camera.lat,
                              "y": camera.lng, "city": camera.city, "fav": 1})
            else:
                jdict.append({"id": camera.id, "address": camera.address, "direct": camera.direct, "x": camera.lat,
                              "y": camera.lng, "city": camera.city, "fav": 0})
    else:
        for camera in cameras:
            jdict.append(
                {"id": camera.id, "address": camera.address, "direct": camera.direct, "x": camera.lat, "y": camera.lng,
                 "city": camera.city})
    context = {'cameras': jdict,
               'title': ' Главная ',
               'city': c,
               'news': n, }
    return render(request, 'index.html', context)


# def about(request):
#     context = {'title': ' О нас  '}
#     return render(request, 'about.html', context)


def about(request):
    context = {'title': ' О нас  '}
    return render(request, 'about.html', context)


def contacts(request):
    context = {'title': ' Контакты '}
    if request.method == "POST":
        name = request.POST['name']
        email = request.POST['email']
        subject = request.POST['subject']
        text = request.POST['text']
        if send_mail(subject + ' ' + name + ' ' + email, text, 'noreply@gorodkamer.ru', ['programmeraxel@gmail.com'],
                     fail_silently=True):
            request.session['alert'] = _(u'Сообщение успешно отправленно')
            context['complete'] = True
        else:
            request.session['alert'] = _(u'В процессе отправки возникла ошибка')
    return render(request, 'contacts.html', context)


# def xcontacts(request):
#     context = {'title': ' Контакты '}
#     if request.method == "POST":
#         name = request.POST['name']
#         email = request.POST['email']
#         subject = request.POST['subject']
#         text = request.POST['text']
#         if send_mail(subject + ' ' + name + ' ' + email, text, 'noreply@gorodkamer.ru', ['programmeraxel@gmail.com'],
#                      fail_silently=True):
#             request.session['alert'] = _(u'Сообщение успешно отправленно')
#             context['complete'] = True
#         else:
#             request.session['alert'] = _(u'В процессе отправки возникла ошибка')
#     return render(request, 'xcontacts.html', context)


def loginv(request):
    if request.method == "POST":
        username = request.POST['login']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                try:
                    if request.POST['next']:
                        #fixme
                        return redirect('gk.views.index')
                        #return redirect(request.POST['next'])
                except:
                    return HttpResponseRedirect(reverse('gk.views.index'))
                return HttpResponseRedirect(reverse('gk.views.index'))
            else:
                request.session['alert'] = 'Ваш аккаунт не активен'
                return HttpResponseRedirect(reverse('gk.views.reg'))
        else:
            request.session['alert'] = 'Неверный логин или пароль'
            return HttpResponseRedirect(reverse('gk.views.reg'))
    else:
        request.session['alert'] = _(u'При выполнении запроса возникла ошибка')
        return HttpResponseRedirect(reverse('gk.views.reg'))


def logoutv(request):
    logout(request)
    return HttpResponseRedirect(reverse('gk.views.index'))


def reg(request):
    if request.method == "POST":
        username = request.POST['login']
        email = request.POST['email']
        password = request.POST['password']
        user, created = User.objects.get_or_create(username=username)
        if created:
            user.set_password(password)
            user.email = email
        else:
            request.session['alert'] = _(u'Данный логин уже используется')
            return HttpResponseRedirect(reverse('gk.views.reg'))
        user.is_active = False
        key = md5(username + 'solt1').hexdigest()
        mailkey = Emailv.objects.get_or_create(user=user)[0]
        mailkey.key = key
        mailkey.save()
        user.save()
        text = _(u'Добро пожаловать на портал Город камер!\n\n'
                 u'Ваша учётная запись ещё не активна. Вы не сможете ей пользоваться,'
                 u' пока не перейдёте по следующей ссылке:\n\n'
                 u'http://gorodkamer.ru/emailv/%s/\n\n'
                 u'Спасибо за то, что зарегистрировались на портале Город камер!') % key
        send_mail(_(u'подтверждение Email gorodkamer.ru'), text, 'noreply@gorodkamer.ru', [email], fail_silently=True)
        return HttpResponseRedirect(reverse('gk.views.index'))
    else:
        try:
            if request.GET['next']:
                context = {'title': ' Регистрация ', 'next': request.GET['next']}
        except:
            context = {'title': ' Регистрация '}
        return render(request, 'reg.html', context)


def fpass(request):
    context = {'title': ' Восстановление пароля '}
    if request.method == "POST":
        username = request.POST['login']
        try:
            user = User.objects.get(username=username)
            rpass = Rpass.objects.get_or_create(user=user)[0]
            #fixme
            key = md5.md5(username).hexdigest()
            rpass.key = key
            rpass.save()
            text = _(u'Для изменения пароля перейдите по ссылке:\n\n'
                     u'http://gorodkamer.ru/sn/%s/\n\n'
                     u'Игнориуйте данное сообщение если вы не востанавливали пароль') % key
            send_mail(_(u'Смена пароля Email gorodkamer.ru'), text, 'noreply@gorodkamer.ru', [user.email],
                      fail_silently=True)
            request.session['alert'] = _(u'Инструкции по смене пароля высланы на ваш Email')
            return HttpResponseRedirect(reverse('gk.views.index'))
        except user.DoesNotExist:
            request.session['alert'] = _(u'Данный пользователь не зарегестрирован')
            return render(request, 'fpass.html', context)
    return render(request, 'fpass.html', context)


def snpass(request, key):
    context = {'title': ' Смена пароля '}
    if request.method == "POST":
        newpass = request.POST['pass']
        newpass1 = request.POST['pass1']
        if newpass == newpass1:
            rpass = get_object_or_404(Rpass, key=key)
            user = rpass.user
            user.set_password(newpass)
            user.save()
            rpass.delete()
            request.session['alert'] = _(u'Пароль успешно изменен.')
            return HttpResponseRedirect(reverse('gk.views.index'))
        else:
            request.session['alert'] = _(u'Пароли не совпадают')
            return render(request, 'snpass.html', context)
    get_object_or_404(Rpass, key=key)
    return render(request, 'snpass.html', context)


# def xsnpass(request):
#     context = {'title': ' Востанволение пароля '}
#     return render(request, 'xsnpass.html', context)


def ads(request):
    context = {'title': ' Реклама на портале '}
    return render(request, 'ads.html', context)


# def xads(request):
#     context = {'title': ' Реклама на портале '}
#     return render(request, 'xads.html', context)


def vote(request, camera_id):
    if request.method == "POST":
        vote = int(request.POST['val'])
        camera = get_object_or_404(Camera, id=camera_id)
        total = camera.votes * camera.rate
        camera.rate = (total + vote) / (camera.votes + 1)
        camera.save()
        jsondict = {'status': 'OK', 'msg': _(u'Спасибо. Ваш голос учтен'), 'votes': camera.votes + 1}
        return HttpResponse(json.dumps(jsondict), mimetype="application/json")
    else:
        jsondict = {'status': 'ERR', 'msg': _(u'Возникла ошибка')}
        return HttpResponse(json.dumps(jsondict), mimetype="application/json")


def allcam(request):
    ccity = City.objects.get(pk=1)
    if "city" in request.COOKIES:
        if request.COOKIES["city"] == '0':
            cameras = Camera.objects.order_by('-views', '-rate').filter(active=True, city=ccity)
        else:
            cameras = Camera.objects.order_by('-views', '-rate').filter(active=True, city=request.COOKIES["city"])
    else:
        cameras = Camera.objects.order_by('-views', '-rate').filter(active=True, city=ccity)
    jdict = []
    if request.user.is_authenticated():
        favcam = Favcam.objects.get_or_create(user=request.user)[0]
        favcams = favcam.cams.all()
        for camera in cameras:
            if camera in favcams:
                jdict.append({"id": camera.id, "address": camera.address, "direct": camera.direct,
                              "x": camera.lat, "y": camera.lng, "fav": 1})
            else:
                jdict.append({"id": camera.id, "address": camera.address, "direct": camera.direct,
                              "x": camera.lat, "y": camera.lng, "fav": 0})
    else:
        for camera in cameras:
            jdict.append({"id": camera.id, "address": camera.address, "direct": camera.direct,
                          "x": camera.lat, "y": camera.lng})
    return HttpResponse(json.dumps(jdict, cls=DecimalEncoder), mimetype="application/json")


def elallcam(request):
    ccity = City.objects.get(pk=4)
    cameras = Camera.objects.order_by('-views', '-rate').filter(active=True, city=ccity)
    jdict = []
    if request.user.is_authenticated():
        favcam = Favcam.objects.get_or_create(user=request.user)[0]
        favcams = favcam.cams.all()
        for camera in cameras:
            if camera in favcams:
                jdict.append({"id": camera.id, "address": camera.address, "direct": camera.direct,
                              "x": camera.lat, "y": camera.lng, "fav": 1})
            else:
                jdict.append({"id": camera.id, "address": camera.address, "direct": camera.direct,
                              "x": camera.lat, "y": camera.lng, "fav": 0})
    else:
        for camera in cameras:
            jdict.append({"id": camera.id, "address": camera.address, "direct": camera.direct,
                          "x": camera.lat, "y": camera.lng})
    return HttpResponse(json.dumps(jdict, cls=DecimalEncoder), mimetype="application/json")


def topcameras(request, step=0):
    mix = int(step) * 3
    if "city" in request.COOKIES:
        if request.COOKIES["city"] == '0':
            cameras = Camera.objects.order_by('-views', 'rate').filter(active=True)[0 + mix:3 + mix]
        else:
            cameras = Camera.objects.order_by('-views', 'rate').filter(active=True,
                                                                       city=request.COOKIES["city"])[0 + mix:3 + mix]
    else:
        cameras = Camera.objects.order_by('-views', 'rate').filter(active=True)[0 + mix:3 + mix]
    jdict = []
    if request.user.is_authenticated():
        favcam = Favcam.objects.get_or_create(user=request.user)[0]
        favcams = favcam.cams.all()
        for camera in cameras:
            if camera in favcams:
                jdict.append({"id": camera.id, "address": camera.address, "direct": camera.direct, "fav": 1,
                              "city": camera.city.name})
            else:
                jdict.append({"id": camera.id, "address": camera.address, "direct": camera.direct, "fav": 0,
                              "city": camera.city.name})
    else:
        for camera in cameras:
            jdict.append(
                {"id": camera.id, "address": camera.address, "direct": camera.direct, "city": camera.city.name})
    return HttpResponse(json.dumps(jdict, cls=DecimalEncoder), mimetype="application/json")


def lastcameras(request):
    if "city" in request.COOKIES:
        if request.COOKIES["city"] == '0':
            cameras = Camera.objects.order_by('-views', '-rate').filter(active=True)[:3]
        else:
            cameras = Camera.objects.order_by('-views', '-rate').filter(active=True, city=request.COOKIES["city"])[:3]
    else:
        cameras = Camera.objects.order_by('-views', '-rate').filter(active=True)[:3]
    jdict = []
    if request.user.is_authenticated():
        favcam = Favcam.objects.get_or_create(user=request.user)[0]
        favcams = favcam.cams.all()
        for camera in cameras:
            if camera in favcams:
                jdict.append({"id": camera.id, "address": camera.address, "direct": camera.direct, "fav": 1})
            else:
                jdict.append({"id": camera.id, "address": camera.address, "direct": camera.direct, "fav": 0})
    else:
        for camera in cameras:
            jdict.append({"id": camera.id, "address": camera.address, "direct": camera.direct})
    return HttpResponse(json.dumps(jdict, cls=DecimalEncoder), mimetype="application/json")


def topcamera(request):
    if "city" in request.COOKIES:
        if request.COOKIES["city"] == '0':
            cameras = Camera.objects.order_by('-views', '-rate').filter(active=True)[:1]
        else:
            cameras = Camera.objects.order_by('-views', '-rate').filter(active=True, city=request.COOKIES["city"])[:1]
    else:
        cameras = Camera.objects.order_by('-views', '-rate').filter(active=True)[:1]
    jdict = []
    if request.user.is_authenticated():
        favcam = Favcam.objects.get_or_create(user=request.user)[0]
        favcams = favcam.cams.all()
        for camera in cameras:
            if camera in favcams:
                jdict.append({"id": camera.id, "address": camera.address, "direct": camera.direct, "fav": 1})
            else:
                jdict.append({"id": camera.id, "address": camera.address, "direct": camera.direct, "fav": 0})
    else:
        for camera in cameras:
            jdict.append({"id": camera.id, "address": camera.address, "direct": camera.direct})
    return HttpResponse(json.dumps(jdict, cls=DecimalEncoder), mimetype="application/json")


def getcity(request):
    citys = City.objects.all()
    jdict = []
    for city in citys:
        jdict.append({"name": city.name, "id": city.id})
    return HttpResponse(json.dumps(jdict, cls=DecimalEncoder), mimetype="application/json")


def addfav(request, camera_id):
    favcam = Favcam.objects.get_or_create(user=request.user)[0]
    try:
        favcam.cams.add(camera_id)
        favcam.save()
    except:
        HttpResponse(1)
    return HttpResponse(0)


def rmfav(request, camera_id):
    favcam = Favcam.objects.get(user=request.user)
    try:
        favcam.cams.remove(int(camera_id))
        favcam.save()
    except:
        HttpResponse(1)
    return HttpResponse(0)


def favcams(request):
    favcam = Favcam.objects.get_or_create(user=request.user)[0]
    favcams = favcam.cams.all()
    jdict = []
    for camera in favcams:
        jdict.append({"id": camera.id, "address": camera.address, "direct": camera.direct})
    return HttpResponse(json.dumps(jdict, cls=DecimalEncoder), mimetype="application/json")


def adver(request):
    return render(request, 'adver.html')