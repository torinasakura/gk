﻿from django.db import models
from django.contrib.auth.models import User


class City(models.Model):
    active = models.BooleanField(verbose_name=u'Активно')
    name = models.CharField(max_length=300, verbose_name=u'Город')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u"Город"
        verbose_name_plural = u"Города"


class Camera(models.Model):
    active = models.BooleanField(verbose_name=u'Активно')
    ip = models.IPAddressField(verbose_name=u'IP')
    city = models.ForeignKey(City, verbose_name=u'Город')
    address = models.CharField(max_length=300, verbose_name=u'Адрес')
    direct = models.CharField(max_length=300, verbose_name=u'Куда смотрит')
    lat = models.DecimalField(max_digits=9, decimal_places=6)
    lng = models.DecimalField(max_digits=9, decimal_places=6)
    votes = models.IntegerField(default=0, verbose_name=u'Голосов')
    rate = models.FloatField(default=0, verbose_name=u'Оценка')
    views = models.IntegerField(default=0, verbose_name=u'Просмотров')

    def __unicode__(self):
        return self.address

    class Meta:
        verbose_name = u"Камера"
        verbose_name_plural = u"Камеры"


class Emailv(models.Model):
    user = models.OneToOneField(User, verbose_name=u'Пользователь')
    key = models.CharField(max_length=32, verbose_name=u'Ключ')

    def __unicode__(self):
        return self.key

    class Meta:
        verbose_name = u"Ключ"
        verbose_name_plural = u"Ключи"


class Rpass(models.Model):
    user = models.OneToOneField(User, verbose_name=u'Пользователь')
    key = models.CharField(max_length=32, verbose_name=u'Ключ')

    def __unicode__(self):
        return self.key

    class Meta:
        verbose_name = u"Rpass"
        verbose_name_plural = u"Rpass"


class Favcam(models.Model):
    user = models.OneToOneField(User, verbose_name=u'Пользователь')
    cams = models.ManyToManyField(Camera, verbose_name=u'Камеры')

    def __unicode__(self):
        return self.user.username

    class Meta:
        verbose_name = u"Избраные камеры"
        verbose_name_plural = u"Избраные камеры"


class Pcam(models.Model):
    user = models.OneToOneField(User, verbose_name=u'Пользователь')
    cams = models.ManyToManyField(Camera, verbose_name=u'Камеры')

    def __unicode__(self):
        return self.user.username

    class Meta:
        verbose_name = u"Личяные камеры"
        verbose_name_plural = u"Личяные камеры"


#class Vote(models.Model):
#    user = models.ForeignKey(User, verbose_name = u'Пользователь')
#    vote = models.IntegerField(default = 0, verbose_name = u'Оценка')
#    def __unicode__(self):
#        return self.user
