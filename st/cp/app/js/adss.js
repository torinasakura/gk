/**
 * Created by GetJudy on 12.03.14.
 */

jQuery(document).ready(function($){
    /*----------------------------------------------------*/
    /*	Pretty Photo
    /*----------------------------------------------------*/
    jQuery("a[data-rel^='prettyPhoto']").prettyPhoto({
        overlay_gallery: false,
        changepicturecallback: function(){
            var h = $(document).height();
            $('.pp_overlay').height(h+1+'px');
            //vk
            VK.Widgets.Like("vk_like", {type: "button", height: 20});
        },
        social_tools: '<div class="twitter"><a href="http://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a></div><div class="facebook"><iframe src="//www.facebook.com/plugins/like.php?locale=en_US&href={location_href}&amp;layout=button_count&amp;show_faces=true&amp;width=500&amp;action=like&amp;font&amp;colorscheme=light&amp;height=23" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:500px; height:23px;" allowTransparency="true"></iframe></div><div id="vk_like"></div>'
    })
    /* Post Image overlay */
    jQuery('.post-image').hover(function () {
        jQuery(this).find( '.img-hover' ).animate({
            "opacity": 0.8
        }, 100);


    }, function () {
        jQuery(this).find( '.img-hover' ).animate({
            "opacity": 0
        }, 100);

    });


    jQuery('.post-image').hover(function () {
        jQuery(this).find(".fullscreen").stop().animate({'top' : '55%', 'opacity' : 1}, 250);

    }, function () {
        jQuery(this).find(".fullscreen").stop().animate({'top' : '65%', 'opacity' : 0}, 150);

    });
});
