/**
 * Created with IntelliJ IDEA.
 * User: GetJudy
 * Date: 19.02.14
 * Time: 15:45
 * To change this template use File | Settings | File Templates.
 */

//Player function
var pv,
    advTimer = false;

jQuery(document).ready(function($){

    /*----------------------------------------------------*/
    /*	Messenger options
    /*----------------------------------------------------*/
    Messenger.options = {
        extraClasses: 'messenger-fixed messenger-on-top',
        theme: 'future'
    }

    /*----------------------------------------------------*/
    /*	getCookie
    /*----------------------------------------------------*/
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    /*----------------------------------------------------*/
    /*	Adding and removing cameras from favorites (Cam List)
    /*----------------------------------------------------*/
    function changeFavCamList(cam){
        var url, img, msg, fav, csrftoken = getCookie('csrftoken');

        if(cam.data('cam-fav') == 1){
            url = "/rmfav/"+cam.data('cam-id')+"/";
            fav = 0;
            img = "plus";
            msg = "Камера успешно удалена"
        }else if(cam.data('cam-fav') == 0){
            url = "/addfav/"+cam.data('cam-id')+"/";
            fav = 1;
            img = "minus";
            msg = "Камера успешно добавлена в избранное";
        }else{
            return false;
        }

        $.ajax({
            type: 'POST',
            url: url,
            data:{csrfmiddlewaretoken:csrftoken},
            dataType: 'json',
            success: function(data){
                if(data == 0){
                    Messenger().post({
                        message: msg,
                        type: 'info',
                        showCloseButton: true
                    });
                    cam.find('.fav-cam').find('i.icon').attr('class','icon ion-'+img+'-round fav-cam').end().data('cam-fav',fav).end().attr('data-cam-fav',fav);
                }
                else{
                    Messenger().post({
                        message: 'Авторизуйтесь',
                        type: 'error',
                        showCloseButton: true
                    });
                }
            }
        })
    }

    /*----------------------------------------------------*/
    /*	Adding and removing cameras from favorites
    /*----------------------------------------------------*/
    function changeFavCam(id,action){
        var url,img,butInfo,msg,nextAction,
            overlayHeight = jQuery('body').height();

        jQuery('.pp_overlay').height(overlayHeight);

        if(action == 'add'){
            url = "/addfav/"+id+"/";
            img = "/st/img/add.png";
            butInfo = "Добавить в избранное";
            msg = "Камера успешно добавлена в избранное";
            nextAction = "remove";
        }
        else if(action == 'remove'){
            url = "/rmfav/"+id+"/";
            img = "/st/img/rem.png";
            butInfo = "Удалить из избранного";
            msg = "Камера успешно удалена";
            nextAction = "add";
        }
        jwplayer().addButton(
            img,
            butInfo,
            function () {
                var currentUrl = url;
                var csrftoken = getCookie('csrftoken');
                $.ajax({
                    type: 'POST',
                    url: currentUrl,
                    data:{csrfmiddlewaretoken:csrftoken},
                    dataType: 'json',
                    success: function(data){
                        if(data == 0){
                            Messenger().post({
                                message: msg,
                                type: 'info',
                                showCloseButton: true
                            });

                            jwplayer().removeButton("fav");
                            changeFavCam(id,nextAction);
                        }
                        else{
                            Messenger().post({
                                message: 'Авторизуйтесь',
                                type: 'error',
                                showCloseButton: true
                            });
                        }
                    }
                })
            },
            "fav"
        )
    }

    /*----------------------------------------------------*/
    /*	Play Video Function
    /*----------------------------------------------------*/
    pv = function(id,address, direct,  fav, notScroll) {
        var fsym = address.charAt(0);

        $('#panel-video').find('.panel-title').html(address + ' <i class="icon ion-arrow-right-c"></i> <i class="text-muted"> '+ direct +'</i>');

        /*//Advertising
        jwplayer("player").setup({
            file: "http://content.bitsontherun.com/videos/3XnJSIm4-kNspJqnJ.mp4",
            image: "/tmb/" + id + ".jpg",
            title: "Онлайн",
            width: '100%',
            aspectratio: "9:5"
        });*/

        //PlayCam
        function playCam(){
            var w = $('#player_container').width(),
                h = w * 0.58;

            $('#panel-video').find('.panel-title').html(address + ' <i class="icon ion-arrow-right-c"></i> <i class="text-muted"> '+ direct +'</i>');

            jwplayer("player").setup({
                sources: [
                    {file: "http://s.gorodkamer.ru:1935/live/" + id + ".stream/manifest.f4m"},
                    {file: "http://s.gorodkamer.ru:1935/live/" + id + ".stream/playlist.m3u8"},
                    {file: "rtmp://s.gorodkamer.ru:1935/live/" + id + ".stream"},
                    {file: "rtmp://s.gorodkamer.ru:1935/live/mp4:" + id + ".stream"},
                    {file: "http://s.gorodkamer.ru:1935/live/mp4:" + id + ".stream/Manifest"},
                ],
                //rtmp: {bufferlength: 3},
                title: "Онлайн",
                image: "/tmb/" + id + ".jpg",
                mute: true,
                width: '100%',
                aspectratio: "9:5",
                autostart: false,//True for Advertising
                position: "center"
            });

            jwplayer().addButton(
                "/st/img/Earth.png",
                'Показать на карте',
                function () {
                    jwplayer().stop();
                    showMarker(id);
                },
                "showCam"
            )

            if(fav == 0){
                changeFavCam(id,'add');
            }
            else if(fav == 1){
                changeFavCam(id,'remove');
            }
        }

        /*GG start*/
        if(fsym == '*'){
            jwplayer("player").setup({
                file: "/tmb/videos/" + id + ".mp4",
                image: "/tmb/" + id + ".jpg",
                mute: true,
                title: "Онлайн",
                width: '100%',
                aspectratio: "9:5"
            });
            jwplayer().onPlay(function(){
                jwplayer().setControls(false);
            });
            jwplayer().onComplete(function(){
                jwplayer().stop();
                prompt('Для продолжения просмотра введите инженерный код активации камеры.', '****');
                alert('Неверный код');
            });
        }
        else{
            playCam();
        }
        /*GG end*/

        if( !notScroll ){
            var top = $('#panel-video').position().top;
            $('.app-body').animate({scrollTop: top}, 'slow');
        }

        //Map extend
        if($('#panel-map').hasClass('expand')){
            $('#panel-map').removeClass('expand');
            $('#map').css({height: heightMap});
            $('.magic-layout').isotope('reLayout');
        }

        /*//Close advertising
        jwplayer().onPause(function(){
            if(advTimer){
                clearTimeout(advTimer);
                advTimer = false;
            }
        })

        jwplayer().onPlay(function(){
            if(advTimer){
                clearTimeout(advTimer);
            }
            advTimer = setTimeout(function(){
                jwplayer().addButton(
                    "/st/img/rem.png",
                    'Закрыть рекламу',
                    function () {
                        playCam();
                    },
                    "closeAdver"
                );
                advTimer = false;
            }, 5000);
        })

        jwplayer().onComplete(function(){
            playCam();
        })*/

        //Magic layout - gn
        setTimeout(function(){
            $('.magic-layout').isotope('reLayout');
        },500)
    }

    /*----------------------------------------------------*/
    /*	Top Camera
    /*----------------------------------------------------*/

    function topCamera() {
        var url = '/topcamera/';
        $.ajax({
            type: "GET",
            url: url,
            dataType: 'json',
            success: function (data) {
                var cam = data[0];
                pv(cam.id, cam.address, cam.direct , cam.fav, true );
            }
        });
    }
    topCamera();

    /*----------------------------------------------------*/
    /*	Play Cam Event
    /*----------------------------------------------------*/
    $('#content-aside').on('click', '.play-cam', function(){
        var cam = $(this).closest('.cam-item');
        pv(cam.data('cam-id'), cam.data('cam-address'), cam.data('cam-direct'), cam.data('cam-fav'));
    })

    /*----------------------------------------------------*/
    /*	Show Marker Event
    /*----------------------------------------------------*/
    $('#content-aside').on('click', '.show-marker', function(){
        var cam = $(this).closest('.cam-item');
        showMarker(cam.data('cam-id'));
    })

    /*------------------------------------------------------------*/
    /*	Adding and removing cameras from favorites (Cam List) Event
    /*------------------------------------------------------------*/
    $('#content-aside').on('click', '.fav-cam', function(){
        var cam = $(this).closest('.cam-item');
        changeFavCamList(cam);
    })

    /*----------------------------------------------------*/
    /*	Yandex Map initialization
    /*----------------------------------------------------*/

    ymaps.ready(init);

    var myMap,
        clusterer,
        heightMap,
        idCam = [],
        geoObject = [];

    function init(){
        myMap = new ymaps.Map ("map", {
            center: [55.61448,38.093159],
            zoom: 12,
            setType:'yandex#map'
        },{
            autoFitToViewport: 'always'
        });

        myMap.behaviors.disable('drag');

        myMap.container.getElement().style.width = '100%';

        heightMap = $('#map').height();

        myMap.container.events.add('sizechange',function(){
            myMap.setBounds(clusterer.getBounds());
        })

        clusterer = new ymaps.Clusterer({
            preset: 'twirl#invertedDarkgreenClusterIcons',
            groupByCoordinates: false,
            clusterDisableClickZoom: false,
            zoomMargin:10
        });

        var myButton = new ymaps.control.Button({
            data: {
                content: '<b style="color:grey">Все камеры</b>'
            }
        }, {selectOnClick: false});

        var url = "/allcam/",
            сamToSearch = [];

        $.getJSON(url,
            function(data) {
                for (var i in data) {
                    var cam = data[i],
                        addresArr = cam.address.split(' '),
                        directArr = cam.direct.split(' ');

                    $.merge(addresArr, directArr);

                    сamToSearch[i] = {
                        value: cam.address+'->'+cam.direct,
                        tokens: addresArr,
                        id:cam.id
                    };

                    idCam[cam.id] = i;
                    geoObject[i] = new ymaps.Placemark([cam.x, cam.y],
                        {
                            balloonContent: '<h4>' + cam.address + '</h4><b>Смотрит на: </b>' + cam.direct+'</br> <a onclick="pv('+cam.id+',\''+cam.address+'\',\''+cam.direct+'\',\''+cam.fav+'\');" href="#">Активировать камеру</a>',
                            id: cam.id,
                            hintContent: cam.address
                        },
                        {preset: 'twirl#darkgreenDotIcon', openBalloonOnClick: false});
                }

                clusterer.add(geoObject);

                clusterer.events.once('objectsaddtomap', function () {
                    myMap.setBounds(clusterer.getBounds());

                    myButton.events.add('click', function () {
                        myMap.setBounds(clusterer.getBounds(),{
                            duration:300
                        });
                    });

                    myMap.controls.add(myButton, {top: 10, left: 10});

                    clusterer.events.add('click',function(e){
                        var target = e.get('target');
                        var id = target.properties.get('id');
                        if(id){
                            showMarker(id);
                        }
                    });
                });

                myMap.geoObjects.add(clusterer);

                /*----------------------------------------------------*/
                /*	Search
                /*----------------------------------------------------*/
                /*var searchForm = jQuery('#search-form');

                searchForm.find('input').typeahead({
                    name: 'cam',
                    local:сamToSearch,
                    template:'<p>{{value}}</p>',
                    engine: Hogan
                });

                searchForm.find('.tt-dropdown-menu').css({'z-index':'99999'});
                searchForm.find('.search-text-box').css({'background-color':'#FFFFFF'});
                searchForm.css({'margin-top':'2px'});
                searchForm.on('typeahead:selected',function(obj, datum){
                    jQuery(".search-text-box").focus();
                    showMarker(datum.id);
                    jQuery(".search-text-box").blur();
                })*/
            }
        );
    };

    /*----------------------------------------------------*/
    /*	Show Marker
    /*----------------------------------------------------*/
    function showMarker(id) {
        var idMarker = idCam[id],
            marker = geoObject[idMarker],
            point = marker.geometry.getCoordinates(),
            top = $('#panel-map').position().top;

        //scroll
        $('.app-body').animate({scrollTop: top}, 'slow');

        myMap.setCenter(point, 25, {
            checkZoomRange: true,
            duration: 200,
            callback: function (err) {
                clusterer.refresh();
                clusterer.events.once('objectsaddtomap',function(){
                    marker.balloon.open();
                });
            }
        });
    }


    /*----------------------------------------------------*/
    /*	Map expand
    /*----------------------------------------------------*/
    $('#panel-map [data-expand]').on('click', function(e){
        var height = heightMap;

        if($('#panel-map').hasClass('panel-collapsed')){
            $('#panel-map').toggleClass('panel-collapsed').children('.panel-body').show();
        }

        if($('#panel-map').hasClass('expand')){
            height = $('#map').parent().height();
        }

        $('#map').css({height: height});
        $('.magic-layout').isotope('reLayout');
    });

    /*----------------------------------------------------*/
    /*	Map collapse
    /*----------------------------------------------------*/
    $('#panel-map [data-collapse]').on('click', function(e){
        if($('#panel-map').hasClass('expand')){
            $('#panel-map').removeClass('expand');
            $('#map').css({height: heightMap});
                $('.magic-layout').isotope('reLayout');
        }
    });

    /*----------------------------------------------------*/
    /*	Ccollapse Title Change
    /*----------------------------------------------------*/
    $('.panel [data-collapse]').on('click', function(e){
        var panel = $(this).closest('.panel');
        if(panel.hasClass('panel-collapsed')){
            $(this).attr('title','Развернуть');
        } else{
            $(this).attr('title','Свернуть');
        }
    });

    /*----------------------------------------------------*/
    /*	Select City
    /*----------------------------------------------------*/
    $('#select_city').find('.city-item').click(function(){
        var cityId = $(this).data('city-id');
        document.cookie="city="+cityId+"; path=/;";
        location.reload();
    })

    /*----------------------------------------------------*/
    /*	Add Cam To List
    /*----------------------------------------------------*/

    var step = 0;
    function addCamTolist(){
        var activeClass = 'item',
            content = '',
            favButton = '',
            url = '/topcameras/'+step+'/';

        $.ajax({
            type: "GET",
            url: url,
            dataType: 'json',
            success: function(data){
                if(data.length < 3){
                    $('.add-cams').hide();
                    if(data.length == 0){
                        return false;
                    }
                }
                step++;
                for (var i in data) {
                    var cam = data[i];
                    if(cam.fav == 1){
                        favButton = '<button type="button" class="btn btn-icon fav-cam"><i class="icon ion-minus-round"></i></button>';
                    } else if(cam.fav == 0){
                        favButton = '<button type="button" class="btn btn-icon fav-cam"><i class="icon ion-plus-round"></i></button>';
                    } else{
                        favButton = '';
                    }
                    content += '<div class="panel panel-default magic-element cam-item" style="margin:10px" data-cam-id=\''+cam.id+'\' data-cam-address=\''+cam.address+'\' data-cam-direct=\''+cam.direct+'\' data-cam-fav=\''+cam.fav+'\'>'+
                        '<div class="panel-heading"><div class="todo-list"><div class="label label-success">Онлайн</div><h3 class="panel-title">'+cam.address+'</h3></div></div>'+
                        '<div class="kits-media play-cam" style="cursor: pointer"><img src="/tmb/'+cam.id+'.jpg" alt="'+cam.address+'" /></div>'+
                        '<div class="panel-footer">'+
                            '<button type="button" class="btn btn-icon play-cam"><i class="icon ion-ios7-play"></i></button>'+
                             favButton+
                            '<button type="button" class="btn btn-icon show-marker"><i class="icon ion-earth"></i></button>'+
                        '</div>'+
                        '<div class="panel-footer"><button type="button" class="btn btn-ion btn-primary btn-lg btn-block" disabled="disabled">'+cam.city+'</button></div>'+
                    '</div>';
                }

                $('#content-aside .cam-box').append(content);
            }
        });
    }
    addCamTolist();

    $('.add-cams').click(function(){
        addCamTolist();
    })

})