/**
 * Created with IntelliJ IDEA.
 * User: GetJudy
 * Date: 08.02.14
 * Time: 15:31
 * To change this template use File | Settings | File Templates.
 */
jQuery(document).ready(function ($) {

    // Messenger
    Messenger.options = {
        extraClasses: 'messenger-fixed messenger-on-top',
        theme: 'future'
    }

    /*----------------------------------------------------*/
    /*	Play Video Function
    /*----------------------------------------------------*/
    function pv(id, name,fav) {
        var w = jQuery("#player_container").width(),
            h = w * 0.58;

        var camName = name.split('->');
        jQuery('#title').html(camName[0]+'<small class="text-muted">'+camName[1]+'</small>');
        console.log('Start: Width = '+w+' Heigth = '+ h);

        jwplayer("player").setup({
            sources: [
                {file: "http://s.gorodkamer.ru:1935/live/" + id + ".stream/manifest.f4m"},
                {file: "http://s.gorodkamer.ru:1935/live/" + id + ".stream/playlist.m3u8"},
                {file: "rtmp://s.gorodkamer.ru:1935/live/" + id + ".stream"},
                {file: "rtmp://s.gorodkamer.ru:1935/live/mp4:" + id + ".stream"},
                {file: "http://s.gorodkamer.ru:1935/live/mp4:" + id + ".stream/Manifest"}
            ],
            //rtmp: {bufferlength: 3},

            title: "Онлайн",
            image: "/tmb/" + id + ".jpg",
            mute: true,
            height: h,
            width: '100%',
            position: "center"
        });

        if(fav == 0){
            changeFavCam(id,'add');
        }
        else if(fav == 1){
            changeFavCam(id,'remove');
        }

        jQuery(window).resize(function(){
            var w = jQuery('#player_wrapper').width();
            var h = w * 0.58;
            console.log('Resize: Width = '+w+' Heigth = '+ h);
            jwplayer().resize('100%',h);
        });

        //ЗК  для  content-aside
        $("#content-aside").bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function(){
            var w = jQuery('#player_wrapper').width();
            var h = w * 0.58;
            console.log('content-aside: Width = '+w+' Heigth = '+ h);
            jwplayer().resize('100%',h);
        })

    }

    /*----------------------------------------------------*/
    /*	getCookie
    /*----------------------------------------------------*/

    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    /*----------------------------------------------------*/
    /*	Adding and removing cameras from favorites
    /*----------------------------------------------------*/
    function changeFavCam(id,action){
        var url,img,butInfo,msg,nextAction,
            callback = false,
            overlayHeight = jQuery('body').height();

        jQuery('.pp_overlay').height(overlayHeight);

        if(action == 'add'){
            url = "/addfav/"+id+"/";
            img = "/st/img/add.png";
            butInfo = "Добавить в избранное";
            msg = "Камера успешно добавлена в избранное";
            nextAction = "remove";
            callback = function(){
                location.reload();
            }
        }
        else if(action == 'remove'){
            url = "/rmfav/"+id+"/";
            img = "/st/img/rem.png";
            butInfo = "Удалить из избранного";
            msg = "Камера успешно удалена";
            nextAction = "add";
            callback = function(id){
                var cam = $('#cams_list').find('[data-cam-id='+id+']'),
                    elements = cam.parent().find('a').length;
                if(elements <= 1){
                    cam.parent().html('<p class="text-muted">У вас нет избранных камер.</p>')
                }
                cam.hide();
            }
        }
        jwplayer().addButton(
            img,
            butInfo,
            function () {
                var currentUrl = url;
                var csrftoken = getCookie('csrftoken');
                $.ajax({
                    type: 'POST',
                    url: currentUrl,
                    data:{csrfmiddlewaretoken:csrftoken},
                    dataType: 'json',
                    success: function(data){
                        if(data == 0){
                            Messenger().post({
                                message: msg,
                                type: 'info',
                                showCloseButton: true
                            });

                            if (callback && typeof(callback) === "function") {
                                callback(id);
                            }

                            jwplayer().removeButton("fav");
                            changeFavCam(id,nextAction);
                        }
                        else{
                            Messenger().post({
                                message: 'Авторизуйтесь',
                                type: 'error',
                                showCloseButton: true
                            });
                        }
                    }
                })
            },
            "fav"
        )
    }

    /*----------------------------------------------------*/
    /*	Add Active Camera
    /*----------------------------------------------------*/

    function topCamera() {
        var url = '/topcamera/';
        $.ajax({
            type: "GET",
            url: url,
            dataType: 'json',
            success: function (data) {
                var cam = data[0];
                var camInfo = cam.address + ' -> ' + cam.direct;
                pv(cam.id, camInfo, cam.fav);
            }
        });
    }
    topCamera();

    /*----------------------------------------------------*/
    /*	Play Cam
    /*----------------------------------------------------*/
    $(document).on('click','#cams_list .cam-item',function(){
        var id = $(this).data('cam-id'),
            name = $(this).data('cam-name');

        pv(id, name, 1);
    });
})