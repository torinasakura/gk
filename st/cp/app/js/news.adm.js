/**
 * Created with IntelliJ IDEA.
 * User: GetJudy
 * Date: 18.02.14
 * Time: 11:25
 * To change this template use File | Settings | File Templates.
 */
jQuery(document).ready(function ($) {

    /*----------------------------------------------------*/
    /*	Messenger
    /*----------------------------------------------------*/
    Messenger.options = {
        extraClasses: 'messenger-fixed messenger-on-top',
        theme: 'future'
    }

    /*----------------------------------------------------*/
    /*	getCookie
    /*----------------------------------------------------*/

    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    /*----------------------------------------------------*/
    /*	Tiny MCE Initialization
    /*----------------------------------------------------*/

    function tinymceInit(selector){
        tinymce.init({
            selector:"textarea[name='"+selector+"']",
            body_id: selector,
            height : 330,
            language : "ru",
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code"
        });
    }

    tinymceInit('news_content');

    /*----------------------------------------------------*/
    /* Tags input
    /*----------------------------------------------------*/
    $('#news_tags').tagsInput({
        height: "32px",
        width:'auto',           // support percent (90%)
        defaultText: 'Метка'
    });

    /*----------------------------------------------------*/
    /* Navigation
    /*----------------------------------------------------*/
    $('nav.side-nav').find('.side-nav-item').click(function(){
        var section = $(this).data('target');
        $(this).siblings().removeClass('active').end().addClass('active');
        $('#content').find('.panel').hide().end().find('#'+section).show();
    })

    /*----------------------------------------------------*/
    /* Table
    /*----------------------------------------------------*/
    $('.datatables').dataTable({
        "iDisplayLength": 10,
        "aaSorting": [],
        "aLengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]
    });

    /*----------------------------------------------------*/
    /* All News Paginator
    /*----------------------------------------------------*/
    var newsPaginator = $('#news_paginator'),
        curentPage = parseInt( newsPaginator.data('curent-page') ),
        numPages = parseInt( newsPaginator.data('num-pages') );

    var options = {
        currentPage: curentPage,
        pageUrl: function(type, page, current){
            return "/news/admin/"+page +"/";
        },
        onPageClicked: function(e,originalEvent,type,page){
            e.stopImmediatePropagation();
        },
        bootstrapMajorVersion: 3,
        numberOfPages:3,
        totalPages: numPages
    }

    newsPaginator.bootstrapPaginator(options);

    /*----------------------------------------------------*/
    /* Select 2
    /*----------------------------------------------------*/
    $(".select2").select2();

    /*----------------------------------------------------*/
    /*	Prewiev
    /*----------------------------------------------------*/
    $('.preview').click(function(){
        var form = jQuery(this).parent(),
            selector = form.find('textarea').attr('name'),
            newsPreview = tinyMCE.get(selector).getContent();

        $('#preview_container').show().find('.panel-body').html(newsPreview);
    })

    /*----------------------------------------------------*/
    /*	Remove Category
    /*----------------------------------------------------*/
    $('#category').on('click','.rm-cat',function(){
        var catId = $(this).closest('tr').data('cat-id'),
            url = "/news/admin/remcat/",
            csrftoken = getCookie('csrftoken');

        alert(catId);

        var msg = Messenger().post({
            message: 'Вы уверены, что хотите удалить категорию?',
            type: 'info',
            showCloseButton: true,
            actions: {
                accept: {
                    label: 'Подтвердить',
                    action: function() {
                        $.ajax({
                            type: "POST",
                            url: url,
                            data:{csrfmiddlewaretoken: csrftoken, cat: catId},
                            dataType: 'json',
                            success: function(data){
                                if(data == 1){
                                    $(this).closest('tr').hide();
                                    return msg.update({
                                        message: 'Категория удалена',
                                        type: 'success',
                                        actions: false,
                                        hideAfter: 2,
                                        hideOnNavigate: true
                                    });
                                } else{
                                    return msg.update({
                                        message: 'Произошла ошибка',
                                        type: 'error',
                                        actions: false,
                                        hideAfter: 2,
                                        hideOnNavigate: true
                                    });
                                }
                            }
                        });
                    }
                }
            }
        });
    });

    /*----------------------------------------------------*/
    /*	Remove News
    /*----------------------------------------------------*/
    $('#all_news').on('click','.rm-news',function(){
        var newsId = $(this).closest('tr').data('news-id'),
            url = "/news/admin/remnews/",
            csrftoken = getCookie('csrftoken');

        var msg = Messenger().post({
            message: 'Вы уверены, что хотите удалить новость?',
            type: 'info',
            showCloseButton: true,
            actions: {
                accept: {
                    label: 'Подтвердить',
                    action: function() {
                        $.ajax({
                            type: "POST",
                            url: url,
                            data:{csrfmiddlewaretoken: csrftoken, news: newsId},
                            dataType: 'json',
                            success: function(data){
                                if(data == 1){
                                    $(this).closest('tr').hide();
                                    return msg.update({
                                        message: 'Новость удалена',
                                        type: 'success',
                                        actions: false,
                                        hideAfter: 2,
                                        hideOnNavigate: true
                                    });
                                } else{
                                    return msg.update({
                                        message: 'Произошла ошибка',
                                        type: 'error',
                                        actions: false,
                                        hideAfter: 2,
                                        hideOnNavigate: true
                                    });
                                }
                            }
                        });
                    }
                }
            }
        });
    })

    /*----------------------------------------------------*/
    /*	Add news form validate
    /*----------------------------------------------------*/
    $('#add_news_form').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        rules: {
            news_priority: {
                required: true,
                min: 0.60,
                max: 0.69
            }
        },

        messages: {
            news_priority: {
                required: 'Пожалуйста, введите поисковый вес новости.',
                min: 'Минимальное значение поискового веса 0.60',
                max: 'Максимальное значение поискового веса 0.69'
            }
        },

        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-success').addClass('has-error');//Заменил has-info на has-success
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-success');//Заменил has-info на has-success
            $(e).remove();
        },
        submitHandler: function(xform) {
            var form = $('#add_news_form'),
                url = form.find('#news_origin_url').val(),
                text = form.find('#news_origin_text').val(),
                link = '<a href="'+url+'" target="_blank">'+text+'</a>';

            form.find('#news_origin').val(link);
            form.find('#news_origin_url').prop('disabled', true);
            form.find('#news_origin_text').prop('disabled', true);
            xform.submit();
        }
    });
})

