/**
 * Created with IntelliJ IDEA.
 * User: GetJudy
 * Date: 27.02.14
 * Time: 16:48
 * To change this template use File | Settings | File Templates.
 */

jQuery(document).ready(function($){
    /*----------------------------------------------------*/
    /*	Pretty Photo
    /*----------------------------------------------------*/
    jQuery("a[data-rel^='prettyPhoto']").prettyPhoto({
        overlay_gallery: false,
        changepicturecallback: function(){
            var h = $(document).height();
            $('.pp_overlay').height(h+1+'px');
        },
        social_tools: '<div class="twitter"><a href="http://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script></div><div class="facebook"><iframe src="//www.facebook.com/plugins/like.php?locale=en_US&href={location_href}&amp;layout=button_count&amp;show_faces=true&amp;width=500&amp;action=like&amp;font&amp;colorscheme=light&amp;height=23" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:500px; height:23px;" allowTransparency="true"></iframe></div><div id="vk_like"></div><script type="text/javascript">VK.Widgets.Like("vk_like", {type: "button", height: 20});</script>' /* html or false to disable */
    });

    /* Post Image overlay */
    jQuery('.post-image').hover(function () {
        jQuery(this).find( '.img-hover' ).animate({
            "opacity": 0.8
        }, 100);


    }, function () {
        jQuery(this).find( '.img-hover' ).animate({
            "opacity": 0
        }, 100);

    });


    jQuery('.post-image').hover(function () {
        jQuery(this).find(".fullscreen").stop().animate({'top' : '55%', 'opacity' : 1}, 250);

    }, function () {
        jQuery(this).find(".fullscreen").stop().animate({'top' : '65%', 'opacity' : 0}, 150);

    });

    /*----------------------------------------------------*/
    /*	Tree menu
    /*----------------------------------------------------*/
    jQuery('.tree-toggle').click(function () {
        var status = jQuery(this).hasClass('tree-open');
        if(status){
            jQuery(this).removeClass('tree-open ion-minus').addClass('tree-close ion-plus');
            $(this).parent().children('ul.tree').hide(200);
        }
        else{
            jQuery(this).removeClass('tree-close ion-plus').addClass('tree-open ion-minus');
            $(this).parent().children('ul.tree').show(200);
        }
    });

    jQuery('.tree-item').click(function(){
        var checkbox = jQuery(this).prev('input:checkbox'),
            checked = checkbox .prop("checked");
        if(checked){
            checkbox.prop("checked",false);
        }
        else{
            checkbox.prop("checked",true);
        }
    })

    jQuery('.category').find('form').submit(function(){
        var category = '';
        jQuery(this).find('input:checkbox:checked').each(function(){
            category += ','+jQuery(this).val();
        })
        category = category.substr(1);
        jQuery(this).find('input[name="category"]').val(category);
    })

    /*----------------------------------------------------*/
    /*	Edit news form validate
    /*----------------------------------------------------*/
    $('#edit_news_form').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        rules: {
            edit_news_priority: {
                required: true,
                min: 0.60,
                max: 0.69
            }
        },

        messages: {
            edit_news_priority: {
                required: 'Пожалуйста, введите поисковый вес новости.',
                min: 'Минимальное значение поискового веса 0.60',
                max: 'Максимальное значение поискового веса 0.69'
            }
        },

        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-success').addClass('has-error');//Заменил has-info на has-success
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-success');//Заменил has-info на has-success
            $(e).remove();
        }
    });
})